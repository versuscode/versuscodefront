import {Component, OnInit} from '@angular/core';
import {Badge} from '../observables/models/badge';
import {BadgeApiService} from './services/badge-api.service';
import {Question} from '../observables/models/question';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-versus',
  templateUrl: './versus.component.html',
  styleUrls: ['./versus.component.css']
})
export class VersusComponent implements OnInit {

  private _questions: Question[] = null;
  private _badges: Badge[];
  private _badgesForm: FormGroup;

  get questions(): Question[] {
    return this._questions;
  }

  set questions(value: Question[]) {
    this._questions = value;
  }

  get badges(): Badge[] {
    return this._badges;
  }

  set badges(value: Badge[]) {
    this._badges = value;
  }

  get badgesForm(): FormGroup {
    return this._badgesForm;
  }

  set badgesForm(value: FormGroup) {
    this._badgesForm = value;
  }

  constructor(private service: BadgeApiService, private builder: FormBuilder) {
  }

  ngOnInit() {
    this.service.getAll().subscribe(
      badges => {
        this.badges = badges;
        // Configuration of the FormGroup
        const formConfig: any = {};
        badges.forEach(function (badge) {
          formConfig[badge.id] = [false, []];
        });
        this.badgesForm = this.builder.group(formConfig);
      },
      (err) => {
        console.log('erreur' + err);
      }
    );
  }

  selectedBadges() {
    const selectedBadges: Array<string> = [];
    for (const v of Object.keys(this.badgesForm.value)) {
      if (this.badgesForm.value[v]) {
        selectedBadges.push(v);
      }
    }
    if (selectedBadges.length > 0) {
      this.service.getQuestionByBadge(selectedBadges).subscribe(
        questions => {
          if (questions.length > 0) {
            this.questions = questions;
          } else if (selectedBadges.length > 1) {
            alert('Pas de questions liées à ces thèmes.');
          } else {
            alert('Pas de questions liées à ce thème.');
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      alert('Pas de thème choisi.');
    }
  }

  gameEndedHandler(event: any) {
    this.questions = event;
  }
}

