import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../observables/models/question';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})

export class QuestionsComponent implements OnInit {

  get question(): any {
    return this._question;
  }

  set question(value: any) {
    this._question = value;
  }

  get compteurQuestion(): number {
    return this._compteurQuestion;
  }

  set compteurQuestion(value: number) {
    this._compteurQuestion = value;
  }

  get questions(): Question[] {
    return this._questions;
  }

  @Input() set questions(value: Question[]) {
    this._questions = value;
  }

  get gameEnded(): EventEmitter<any> {
    return this._gameEnded;
  }

  @Output() set gameEnded(value: EventEmitter<any>) {
    this._gameEnded = value;
  }

  get points(): number {
    return this._points;
  }

  set points(value: number) {
    this._points = value;
  }

  constructor() {
  }

  private _question: any = null;
  private _compteurQuestion = 0;
  private _questions: Question[];
  private _points = 0;
  private _gameEnded: EventEmitter<any> = new EventEmitter();

  static shuffling(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;  // While there remain elements to shuffle...
    while (0 !== currentIndex) {    // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;    // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  ngOnInit() {
    this.questions = QuestionsComponent.shuffling(this.questions);
    this.genererQuestion();
  }

  genererQuestion() {
    this.question = this.questions[this._compteurQuestion];
    this.question.mauvaisesReponses = QuestionsComponent.shuffling(this.question.mauvaisesReponses);
    this.question.reponses = [
      this.question.bonneReponse,
      this.question.mauvaisesReponses[0],
      this.question.mauvaisesReponses[1],
      this.question.mauvaisesReponses[2]
    ];
    this.question.reponses = QuestionsComponent.shuffling(this.question.reponses);
  }

  /**
   * Asynchronous function to delay next question generation
   * async keyword necessary to use another async function
   */
  async valider(r: number) {
    // Check the answer
    this.question.reponses[r].repondu = true;
    if (this.question.reponses[r].id === this.question.bonneReponse.id) {
      this.points++;
    }
    await this.delay(2000);
    // Generate next question or finish the game
    this._compteurQuestion++;
    if (this._compteurQuestion < 5 && this._compteurQuestion < this.questions.length) {
      this.genererQuestion();
    } else if (this._compteurQuestion === 5) {
      this.endGame('Jeu terminé. Vous avez : ' + this._points + '_pointss.');
    } else {
      this.endGame('Plus assez de questions. Vous avez : ' + this._points + ' points.');
    }
  }

  private endGame(message: string) {
    alert(message);
    this.gameEnded.emit(null);
  }

  /**
   * Asynchronous function to delay next code line
   * async keyword necessary to use a Promise
   */
  async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
