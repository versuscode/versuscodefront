import {Reponse} from '../../versus/models/reponse';
import {Badge} from './badge';

export class Question {

  constructor(
    private _id: number,
    private _intitule: string,
    private _badge: Badge,
    private _bonneReponse: Reponse,
    private _mauvaisesReponses: Reponse[],
    private _reponses: Reponse[]
  ) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get intitule(): string {
    return this._intitule;
  }

  set intitule(value: string) {
    this._intitule = value;
  }

  get badge(): Badge {
    return this._badge;
  }

  set badge(value: Badge) {
    this._badge = value;
  }

  get bonneReponse(): Reponse {
    return this._bonneReponse;
  }

  set bonneReponse(value: Reponse) {
    this._bonneReponse = value;
  }

  get mauvaisesReponses(): Reponse[] {
    return this._mauvaisesReponses;
  }

  set mauvaisesReponses(value: Reponse[]) {
    this._mauvaisesReponses = value;
  }

  get reponses(): Reponse[] {
    return this._reponses;
  }

  set reponses(value: Reponse[]) {
    this._reponses = value;
  }
}
