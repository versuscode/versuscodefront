import {Badge} from './badge';

export class User {
  private _position: number;

  constructor(
    private _id: number,
    private _pseudo: string,
    private _level: number,
    private _badges: Badge[]
  ) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get pseudo(): string {
    return this._pseudo;
  }

  set pseudo(value: string) {
    this._pseudo = value;
  }

  get level(): number {
    return this._level;
  }

  set level(value: number) {
    this._level = value;
  }

  get badges(): Badge[] {
    return this._badges;
  }

  set badges(value: Badge[]) {
    this._badges = value;
  }

  get position(): number {
    return this._position;
  }

  set position(value: number) {
    this._position = value;
  }
}
