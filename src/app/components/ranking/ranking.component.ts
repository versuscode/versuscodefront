import {Component, OnInit} from '@angular/core';
import {User} from '../observables/models/user';
import {UserApiService} from '../observables/services/user-api.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {
  get users(): User[] {
    return this._users;
  }

  set users(value: User[]) {
    this._users = value;
  }

  constructor(private service: UserApiService) {
  }

  private _users: User[] = null;

  static findEqualPoints(users: User[]) {
    let level = -1;
    let position = 1;
    let c = 1;
    for (const user of users) {
      if (user.level === level) {
        user.position = position;
      } else {
        user.position = c;
        position = c;
      }
      level = user.level;
      c++;
    }
  }

  ngOnInit() {
    this.service.getAll().subscribe(users => {
      // Sort by level
      users.sort((u1, u2) => {
          if (u1.level === u2.level) {
            return 0;
          } else {
            return (u1.level > u2.level) ? -1 : 1;
          }
        }
      );
      // Find equal players
      RankingComponent.findEqualPoints(users);
      this._users = users;
    });
  }
}
